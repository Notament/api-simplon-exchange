<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use App\User;

class UserController extends Controller {

    use Helpers;

    public function getUsers() {

        $users = User::all();

        return $this->response->collection($users, new UserTransformer);
    }

    public function show($id) {
        $user = User::findOrFail($id);
        return $this->response->item($user, new UserTransformer);
    }

    public function destroy($id) {
        
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return $this->response->noContent();
        }
        catch(Exception $e) {
            return $this->response->error('error', 500);
        }

    }

    // public function update(Request $request) {
    //     $user = User::findOrFail()
    // }
}
<?php

namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $user = DB::table('users')->where('email', $request->input('email'))->where('password', $request->input('password'))->limit(1)->get();
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$user) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        $token = JWTAuth::fromUser($user[0]);
        return response()->json($token);
    }

}
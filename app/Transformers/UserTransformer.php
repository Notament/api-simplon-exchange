<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'name' => $user->name,
            'email' => $user->email,
            'password'=> $user->password,
            'is_admin'=> $user->is_admin,
            'remember_token'=> $user->remember_token,
            'fabric_id'=> $user->fabric_id,
            // 'created_at' => date('Y-m-d', strtotime($user->created_at)),
            // 'updated_at'=> date('Y-m-d', strtotime($user->updated_at))
        ];
    }
}
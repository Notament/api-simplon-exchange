<?php

use Illuminate\Http\Request;


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [], function ($api) {
    $api->group(['namespace'=>'App\Http\Controllers','middleware'=>['jwt.auth']], function ($api){
        $api->get('/users', 'UserController@getUsers');
        $api->get('/users/{id}', 'UserController@show');
        $api->delete('/users/{id}', 'UserController@destroy');
       
    });

    $api->group(['namespace'=>'App\Http\Controllers'], function ($api){
       
        $api->post('/login', 'AuthenticateController@authenticate');
    });



});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
